/**
 * 
 */
package com.domain;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Objeto que representa uma operacao de transacao.
 * 
 * @author Vinicius N. R. Garcia
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Operation {

    /**
     * Numero operacao.
     */
    private Long nuOperation;

    /**
     * Usuario.
     */
    private String user;

    /**
     * Valor liquido.
     */
    private BigDecimal netValue;

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Operation [nuOperation=" + nuOperation + ", user=" + user + ", netValue=" + netValue + "]";
    }

}