package com.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RestController;

import com.kafka.config.KafkaConsumerConfiguration;
import com.web.resources.kafkaMessageResource;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Vinicius N. R. Garcia
 *
 */
@Slf4j
@RestController
@SpringBootApplication
@ComponentScan(basePackageClasses = { KafkaConsumerConfiguration.class, kafkaMessageResource.class })
public class SpringBootKafkaTestApplication implements CommandLineRunner {

    @Autowired
    public transient kafkaMessageResource resource;

    public static void main(String[] args) {
        SpringApplication.run(SpringBootKafkaTestApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Inicializando método run...");
        this.resource.sendMessages();
        // this.resource.sendMessageWithCallback("message with callback");
    }

}