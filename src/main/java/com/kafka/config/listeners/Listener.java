package com.kafka.config.listeners;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author vnrg
 *
 */
@Slf4j
@Component
public class Listener {

    /** Topico */
    private static final String TOPIC_NAME = "topic1";

    @Value("${size.messages}")
    public int sizeMessages;

    // /** Total mensagens */
    // private static final long MESSAGES = 1000;

    @Getter
    @Setter
    private transient List<String> messages;

    public CountDownLatch countDownLatch0 = new CountDownLatch(3);
    /*
     * public CountDownLatch countDownLatch1 = new CountDownLatch(3); public
     * CountDownLatch countDownLatch2 = new CountDownLatch(3);
     */

    @KafkaListener(id = "id0", group = "group102", topicPartitions = {
            @TopicPartition(topic = TOPIC_NAME, partitions = { "0", "1", "2" }) })
    public void listenPartition0(ConsumerRecord<?, ?> record) {
//        log.info("Listener partition: " + record.partition() + ", Thread ID: " + Thread.currentThread().getId()
//                + " Received: " + record + " Partition: " + record.partition() + " Topic: " + record.topic());
        this.countDown(this.countDownLatch0, record);
    }

    /*
     * @KafkaListener(id = "id1", group = "group102", topicPartitions = {
     * 
     * @TopicPartition(topic = TOPIC_NAME, partitions = { "1" }) }) public void
     * listenPartition1(ConsumerRecord<?, ?> record) {
     * log.info("Listener Id1, Thread ID: " + Thread.currentThread().getId() +
     * " Received: " + record + " Partition: " + record.partition() + " Topic: " +
     * record.topic()); this.countDown(this.countDownLatch1, record); }
     * 
     * @KafkaListener(id = "id2", group = "group102", topicPartitions = {
     * 
     * @TopicPartition(topic = TOPIC_NAME, partitions = { "2" }) }) public void
     * listenPartition2(ConsumerRecord<?, ?> record) {
     * log.info("Listener Id2, Thread ID: " + Thread.currentThread().getId() +
     * " Received: " + record + " Partition: " + record.partition() + " Topic: " +
     * record.topic()); this.countDown(this.countDownLatch2, record); }
     */

    /**
     * Implementa o método {@code countDown} e adiciona a mensagens na lista.
     * 
     * @param countDownLatch
     *            {@link CountDownLatch}
     * @param record
     *            {@link ConsumerRecord}
     */
    private void countDown(final CountDownLatch countDownLatch, ConsumerRecord<?, ?> record) {
        this.checkEndMessages(String.valueOf(record.value()));
        countDownLatch.countDown();
    }

    @Async
    public void checkEndMessages(final String message) {

        final long t = System.currentTimeMillis();
        if (this.messages == null) {
            log.info("Inicio recebimento das mensagens: " + LocalDateTime.now());
            this.messages = new ArrayList<String>();
        }

        this.messages.add(message);

        if (this.messages.size() == sizeMessages) {
            log.info("Total mensagens recebidas: " + this.messages.size());
            log.info("Fim recebimento das mensagens (ms): " + (System.currentTimeMillis() / t));
            this.messages.clear();
        }
    }
}
