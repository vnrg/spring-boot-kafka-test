/**
 * 
 */
package com.web.resources;

import java.math.BigDecimal;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.domain.Operation;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * @author vnrg
 *
 */
@Slf4j
@RestController
public class kafkaMessageResource {

    // /** Total mensagens */
    // private static final long MESSAGES = 1000;

    @Value("${topic.json}")
    public String topic;

    @Value("${size.messages}")
    public int sizeMessages;

    @Autowired
    public KafkaTemplate<String, String> kafkaTemplate;

    @PostMapping
    public ResponseEntity<String> sendMessage(@RequestParam(value = "message") String message) {
        this.kafkaTemplate.send(this.topic, message);
        log.info("Mensagem enviada para fila!");
        return ResponseEntity.noContent().build();
    }

    @PostMapping(path = "messages")
    public ResponseEntity<String> sendMessages() {
        long t = System.currentTimeMillis();
        String messageJson = null;

        try {

            ObjectMapper mapper = new ObjectMapper();
            messageJson = mapper.writeValueAsString(new Operation());

        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
        }

        for (long i = 0; i < sizeMessages; i++) {
            this.kafkaTemplate.send(this.topic, "Mensagem " + i + " json: " + messageJson);
        }
        log.info("Mensagens enviadas para fila!");
        log.info("Tempo para envio das mensagems (ms): " + (System.currentTimeMillis() / t));
        return ResponseEntity.noContent().build();
    }

    public void sendMessageWithCallback(final String message) {
        ProducerRecord<String, String> record = new ProducerRecord<String, String>(this.topic,
                (new Operation(1L, "user", BigDecimal.ONE).toString()));

        ListenableFuture<SendResult<String, String>> future = this.kafkaTemplate.send(record);

        future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {

            @Override
            public void onSuccess(SendResult<String, String> arg0) {
                log.info("Mensage enviada e recebida com sucesso! " + arg0.getProducerRecord());
            }

            @Override
            public void onFailure(Throwable arg0) {
                log.error("Erro ao enviar mensagem para fila!" + arg0.getStackTrace());
            }
        });
    }

}
